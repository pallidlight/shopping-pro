package cn.gdpu.view;

import cn.gdpu.domain.UserShop;
import cn.gdpu.domain.UserShopTail;
import cn.gdpu.service.ServiceDao;
import cn.gdpu.service.impl.ServiceDaoImpl;
import cn.gdpu.util.JFrameBackground;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.List;

public class UserShopCheck{
    
    private ServiceDao serviceDao = new ServiceDaoImpl();
    
    public UserShopCheck(List<UserShop> list){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
        OnlineStore.InitGlobalFont(new Font("楷体",Font.BOLD,16));
        JFrame jf = new JFrame("购物记录");
        JPanel jp = new JPanel();
        jp.setOpaque(false);
        jf.setBounds(400,200,1100,500);
        Toolkit tk = Toolkit.getDefaultToolkit();
        Image i = tk.getImage("src/main/resources/images/titlePicture.jpg");
        jf.setIconImage(i);
        new JFrameBackground(jf , "src/main/resources/images/bgPicture.jpg");
        
        jf.add(jp);
        jp.setLayout(new GridLayout(0,4));
        jp.add(new JLabel("         id"));
        jp.add(new JLabel("  订单号"));
        jp.add(new JLabel("    花费"));
        jp.add(new JLabel("  日期"));
        if(list != null){
            for(UserShop us : list){
                JTextField jt = new JTextField(us.getList());
                jt.setEditable(false);
                jt.setOpaque(false);
                JTextField jt2 = new JTextField(sdf.format(us.getDate()));
                jt2.setEditable(false);
                jt2.setOpaque(false);
                jt.addMouseListener(new MouseAdapter(){
                    @Override
                    public void mouseClicked(MouseEvent e){
                        try{
                            List<UserShopTail> tails = serviceDao.findUserShopTail(jt.getText());
                            new UserShopTailCheck(tails);
                        }catch(Exception ex){
                            ex.printStackTrace();
                        }
                    }
                });
                jp.add(new JLabel("         "+us.getUid()));
                jp.add(jt);
                jp.add(new JLabel("    "+us.getCost()));
                jp.add(jt2);
            }
        }
        jf.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e){
                jf.dispose();
            }
        });
        jf.setVisible(true);
    }

    //public static void main(String[] args) throws Exception{
    //    ServiceDao serviceDao = new ServiceDaoImpl();
    //    List<UserShop> list = serviceDao.findAll(1);
    //    new UserShopCheck(list);
    //}
}
