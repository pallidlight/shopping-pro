package cn.gdpu.view;

import cn.gdpu.domain.UserShop;
import cn.gdpu.domain.UserShopTail;
import cn.gdpu.service.ServiceDao;
import cn.gdpu.service.impl.ServiceDaoImpl;
import cn.gdpu.util.JFrameBackground;

import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.List;

public class UserShopTailCheck{
    
    public UserShopTailCheck(List<UserShopTail> list){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
        OnlineStore.InitGlobalFont(new Font("楷体",Font.BOLD,16));
        JFrame jf = new JFrame("购物详细记录");
        JPanel jp = new JPanel();
        jp.setOpaque(false);
        jf.setBounds(430,230,1100,550);
        Toolkit tk = Toolkit.getDefaultToolkit();
        Image i = tk.getImage("src/main/resources/images/titlePicture.jpg");
        jf.setIconImage(i);
        
        String[] titles = {"用户id","商品id","商品名","单价","数量","日期","订单号"};
        String[][] values = new String[list.size()][7];
        if(list != null){
            int count = 0;
            for(UserShopTail us : list){
                values[count][0] = ""+us.getUid();
                values[count][1] = ""+us.getGid();
                values[count][2] = ""+us.getGname();
                values[count][3] = ""+us.getGprice();
                values[count][4] = ""+us.getGquatity();
                values[count][5] = ""+sdf.format(us.getDate());
                values[count][6] = ""+us.getList();
                count++;
            }
        }
        JTable table = new JTable(values,titles);
        JScrollPane sp = new JScrollPane(table);
        //table.add(null,titles);
        //jp.setPreferredSize(new Dimension(jf.getWidth()-100,jf.getHeight()-100));
        //table.setPreferredSize(new Dimension(980,480));
        table.setPreferredSize(new Dimension(980,600));
        
        sp.setPreferredSize(new Dimension(980,460));
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        sp.setOpaque(false);
        table.setOpaque(false);
        jp.setOpaque(false);
        jp.add(sp);
        jf.add(jp);
        
        jf.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e){
                jf.dispose();
            }
        });
        //sizeWindowOnScreen(jf, 0.6, 0.6);
        jf.setVisible(true);
        //jf.setResizable(true);
    }
    
    /**
     *  动态比例
     * @param widthRate 宽度比例 
     * @param heightRate 高度比例
     */
    private void sizeWindowOnScreen(JFrame jf, double widthRate, double heightRate)
    {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        jf.setSize(new Dimension((int) (screenSize.width * widthRate),
                (int) (screenSize.height * heightRate)));
    }

    public static void main(String[] args) throws Exception{
        ServiceDao serviceDao = new ServiceDaoImpl();
        List<UserShopTail> list = serviceDao.findUserShopTail("106f97469a734acbb6a77ccb138b014f");
        new UserShopTailCheck(list);
    }
}
