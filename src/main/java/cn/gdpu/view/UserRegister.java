package cn.gdpu.view;

import cn.gdpu.domain.User;
import cn.gdpu.service.ServiceDao;
import cn.gdpu.service.impl.ServiceDaoImpl;
import cn.gdpu.util.CheckCode;
import cn.gdpu.util.JFrameBackground;

import java.awt.*;
import java.awt.event.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;

public class UserRegister{

	private JTextField text1 = new JTextField(12);
	private JPasswordField text2 = new JPasswordField(12);
	private JPasswordField text3 = new JPasswordField(12);
	private JTextField text5 = new JTextField(5);
	private JLabel label = new JLabel();
	private String checkCode;

	private ServiceDao serviceDao = new ServiceDaoImpl();

	public UserRegister(){
		final JFrame JF = new JFrame("注册界面");
		Toolkit tk = Toolkit.getDefaultToolkit();
		Image i = tk.getImage("src/main/resources/images/user.jpg");
		JF.setIconImage(i);

		JF.setBounds(600 , 400 , 300 , 300);
		JF.setLayout(new FlowLayout(FlowLayout.CENTER)); //设置外框架为居中对齐
		JPanel jp = new JPanel(new FlowLayout(FlowLayout.CENTER));//设置内框架jp为居中对齐

		//生成背景图片
		new JFrameBackground(JF , "src/main/resources/images/bgPicture.jpg");
		//新建2个面板
		JPanel jp1 = new JPanel();
		JPanel jp2 = new JPanel();
		JPanel jp3 = new JPanel();
		JPanel jp4 = new JPanel();
		//面板透明化
		jp1.setOpaque(false);
		jp2.setOpaque(false);
		jp3.setOpaque(false);
		//设置对应的标签
		JLabel jl1 = new JLabel("用户名");
		jl1.setFont(new Font("楷体" , Font.BOLD , 16));
		JLabel jl2 = new JLabel("密  码");
		jl2.setFont(new Font("楷体" , Font.BOLD , 16));
		JLabel jl3 = new JLabel("确认密码");
		jl3.setFont(new Font("楷体" , Font.BOLD , 16));
		JLabel jl4 = new JLabel("验证码");
		jl4.setFont(new Font("楷体" , Font.BOLD , 16));
		//设置对应的文本域大小

		//标签与文本域加入面板之中
		jp1.add(jl1);
		jp1.add(text1);
		jp2.add(jl2);
		jp2.add(text2);
		jp3.add(jl3);
		jp3.add(text3);
		jp4.add(jl4);


		//定义注册日期
		final DateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分");

		//添加验证码的图
		//获取随机值
		checkCode = CheckCode.getNum();
		label.setIcon(new ImageIcon(CheckCode.checkCode(checkCode)));
		jp3.add(label);
		label.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				checkCode = CheckCode.getNum();
				label.setIcon(new ImageIcon(CheckCode.checkCode(checkCode)));
			}
		});
		jp4.add(label);
		jp4.add(text5);

		JButton jb1 = new JButton("注册");
		jb1.setFont(new Font("楷体" , Font.BOLD , 16));
		jb1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				// TODO Auto-generated method stub
				// 给出提示
				try{
					if(judgeRegister() == 1){
						JOptionPane.showMessageDialog(null , "用户在" + df.format(new Date()) + "注册成功!" , "提示" , JOptionPane.INFORMATION_MESSAGE);
						JF.dispose();
						new LoginAndRegister();
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		});
		JButton jb2 = new JButton("取消");
		jb2.setFont(new Font("楷体" , Font.BOLD , 16));
		jb2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JF.dispose();
				new LoginAndRegister();
			}
		});
		JF.add(jp1);
		JF.add(jp2);
		JF.add(jp3);
		JF.add(jp4);
		jp.add(jb1);
		jp.add(jb2);
		JF.add(jp);
		//创建窗口监听
		JF.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				System.exit(0);
				//JF.dispose();
				//new LoginAndRegister();
			}
		});
		JF.setVisible(true);
	}

	/**
	 * 注册验证
	 * @return
	 * @throws Exception
	 */
	public int judgeRegister() throws Exception{

		String username = text1.getText().trim();
		String password = text2.getText().trim();
		String rpassword = text3.getText().trim();
		String captcha = text5.getText().trim();
		int count=0;
		// 用户名规则
		String usernameRegex = "^[a-zA-Z][a-zA-Z0-9]{5,9}$"; //必须以字母开头 的6-10位长度
		// 密码规则
		String passwordRegex = "^[A-Za-z0-9]{6,13}+$";  //匹配由数字和26个英文字母组成的字符串
		// 校验
		if(!username.matches(usernameRegex)){
			JOptionPane.showMessageDialog(null , "用户名不满足条件(6-10长度且以字母开头)" , "提示" , JOptionPane.ERROR_MESSAGE);
			text1.setText("");     //重置用户名 以便于让用户重新输入
			text1.requestFocus();
			return -1;
		}else if(!password.matches(passwordRegex)){
			JOptionPane.showMessageDialog(null , "密码不满足条件(6-13个任意单词字符)" , "提示" , JOptionPane.ERROR_MESSAGE);
			text2.setText("");     //重置密码框文本，以便于让用户重新输入
			text3.setText("");
			text2.requestFocus();
			return -1;
		}else if(!(password.equals(rpassword))){
			JOptionPane.showMessageDialog(null , "两次密码不一致，请重新输入!" , "提示" , JOptionPane.ERROR_MESSAGE);
			text2.setText("");
			text3.setText("");
			text2.requestFocus();
			return -1;
		}else if(!LoginAndRegister.judgeCheckCode(checkCode , captcha)){
			JOptionPane.showMessageDialog(null , "验证码错误" , "提示" , JOptionPane.ERROR_MESSAGE);
			checkCode = CheckCode.getNum();
			label.setIcon(new ImageIcon(CheckCode.checkCode(checkCode)));
			text5.setText("");       //对应的要输入验证码的区域设为空
			text5.requestFocus();    //调整注意位置
		}else{
			//封装用户
			final User user = new User();
			user.setName(username);
			user.setPassword(password);
			count = serviceDao.insert(user);
		}
		return count;
	}

    /*public static void main(String[] args){
        new UserRegister();
    }*/

}
