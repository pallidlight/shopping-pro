package cn.gdpu.view;

import cn.gdpu.domain.Good;
import cn.gdpu.domain.PageBean;
import cn.gdpu.domain.User;
import cn.gdpu.domain.UserCart;
import cn.gdpu.service.ServiceDao;
import cn.gdpu.service.impl.ServiceDaoImpl;
import cn.gdpu.util.DrawPanel;
import cn.gdpu.util.JFrameBackground;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

public class ShoppingCart{
    private static JLabel title1 = new JLabel();
    private static JLabel title2 = new JLabel();
    private static JLabel title3 = new JLabel();
    private static JLabel title4 = new JLabel();
    private static JLabel title5 = new JLabel();
    private static JLabel title6 = new JLabel();

    private static int currentPage = 1;
    private static int totalPage;
    private static int totalCount;
    private static int cartCurrentPage = 1;
    private static int cartTotalPage;

    private static ServiceDao serviceDao = new ServiceDaoImpl();

    //private static User user = new User();
    //
    public ShoppingCart(User user) throws Exception{
        //public static void main(String[] args) throws Exception{
        //    //类测试变量
        //    user.setId(1);
        //    user.setPassword("123456");

        JButton pro = new JButton("上一页");
        JButton next = new JButton("下一页");
        JButton cartPro = new JButton("上一页");
        JButton cartNext = new JButton("下一页");
        OnlineStore.InitGlobalFont(new Font("楷体" , Font.BOLD , 16));
        JLabel label = new JLabel();
        FlowLayout f = new FlowLayout();
        f.setHgap(0);
        f.setVgap(0);
        final JFrame jf = new JFrame("购物车界面");
        jf.setBounds(400 , 200 , 950 , 730);//设计窗口大小 600
        jf.setLayout(new FlowLayout());//设置窗口布局为流布局
        Toolkit tk = Toolkit.getDefaultToolkit();
        Image i = tk.getImage("src/main/resources/images/shoppingCartPicture.jpg");
        jf.setIconImage(i);

        //生成背景图片
        new JFrameBackground(jf , "src/main/resources/images/bgPicture.jpg");

        JPanel jp0 = new JPanel();
        JPanel jp1 = new JPanel();
        JPanel jp2 = new JPanel();
        JPanel jp3 = new JPanel();
        JPanel jp4 = new JPanel();
        JPanel jp5 = new JPanel();
        JPanel jp6 = new JPanel();//存放jp3的面板
        JPanel jp7 = new JPanel();
        JPanel jp8 = new JPanel();
        JPanel jp9 = new JPanel();
        JPanel jp00 = new JPanel();
        JPanel jp01 = new JPanel();
        JPanel jpp3 = new JPanel();

        //面板透明化
        jp0.setOpaque(false);
        jp1.setOpaque(false);
        jp2.setOpaque(false);
        jp3.setOpaque(false);
        jp4.setOpaque(false);
        jp5.setOpaque(false);
        jp6.setOpaque(false);
        jp7.setOpaque(false);
        jp8.setOpaque(false);
        jp9.setOpaque(false);
        jp00.setOpaque(false);
        jp01.setOpaque(false);
        jpp3.setOpaque(false);

        //先new需要的文本域及对应的滚动条
        JTextArea jta = new JTextArea();
        jta.setEditable(false);
        jta.setPreferredSize(new Dimension(145 , 350));
        jta.setLineWrap(true);        //激活自动换行功能 
        jta.setWrapStyleWord(true);            // 激活断行不断字功能
        jta.setBackground(Color.LIGHT_GRAY);

        JScrollPane jsp = new JScrollPane(jta);//生成滚动条
        jsp.setPreferredSize(new Dimension(147 , 150));
//			jsp.getVerticalScrollBar().setUnitIncrement(1); //每点击knob一次滚动十个象素
        jsp.getVerticalScrollBar().setBlockIncrement(10); //每点击track一次滚动五象素
        jsp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER); //水平滚动条不显示
        jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED); //自动显示竖直滚动条
        jta.setText("本广告由dnf冠名赞助,现在前往dnfqq.com注册新账号并在一个星期内达到" +
                "满级即可领取自选苍穹武器礼盒及超界防具五件套礼盒、95泰伯尔斯自选礼盒" +
                "更有一个月黑钻免费领取。更多详情请登录www.bilibili.com查看!");

        //设置图片
        JPanel jpt = new JPanel();
        jpt.setLayout(new GridLayout(1 , 0));
        jpt.setPreferredSize(new Dimension(120 , 120));
        DrawPanel my = null;
        my = new DrawPanel();
        jpt.add(my);
        jp1.setPreferredSize(new Dimension(930 , 110));
        jp1.setBorder(BorderFactory.createLineBorder(null , 1)); //设置边框粗细
        //设置标题
        JLabel jl1 = new JLabel("我的购物车");
        jl1.setFont(new Font("楷体" , Font.BOLD , 35));//设置标题字体大小
        jp0.add(jl1);

        jp1.add(jpt);
        jp1.add(jp0);
        jf.add(jp1);

        //中间词条说明
        jp2.setPreferredSize(new Dimension(930 , 35));
        jp2.setLayout(new FlowLayout(FlowLayout.LEFT));
        title1.setText("商品名称 |");
        title2.setText("数量 |");
        title3.setText("市场价 |");
        title4.setText("会员价 |");
        title5.setText("折扣 |");
        title6.setText("小计 |");
        title1.setFont(new Font("楷体" , Font.BOLD , 16));
        title2.setFont(new Font("楷体" , Font.BOLD , 16));
        title3.setFont(new Font("楷体" , Font.BOLD , 16));
        title4.setFont(new Font("楷体" , Font.BOLD , 16));
        title5.setFont(new Font("楷体" , Font.BOLD , 16));
        title6.setFont(new Font("楷体" , Font.BOLD , 16));
        jp2.add(title1);
        jp2.add(title2);
        jp2.add(title3);
        jp2.add(title4);
        jp2.add(title5);
        jp2.add(title6);
        JButton jb2 = new JButton("购物记录");
        jb2.setPreferredSize(new Dimension(125 , 25));
        jb2.setBackground(Color.orange);
        jp2.add(jb2);

        JTextField jt = new JTextField(12);
        jt.setText("请输入商品名");
        jt.setPreferredSize(new Dimension(150 , 25));
        jp2.add(jt);
        JButton jb = new JButton("立即查找");
        jb.setFont(new Font("楷体" , Font.BOLD , 16));
        jb.setPreferredSize(new Dimension(110 , 25));
        jp2.add(jb);
        JButton jb3 = new JButton("返回主页面");
        jb3.setFont(new Font("楷体" , Font.BOLD , 16));
        jb3.setPreferredSize(new Dimension(150 , 25));
        jb3.setBackground(Color.orange);
        jp2.add(jb3);
        jf.add(jp2);

        //我的购物车模块
        jp6.setPreferredSize(new Dimension(935 , 290));
        jp6.setLayout(new FlowLayout());
        jp6.setBorder(BorderFactory.createLineBorder(null , 0));
        jp6.add(jpp3);
        jp6.add(jp4);

        jpp3.add(jp3);
        jpp3.setPreferredSize(new Dimension(755 , 280));
        jpp3.setBorder(BorderFactory.createLineBorder(null , 1));
        jp3.setPreferredSize(new Dimension(745 , 230));
        jp3.setBorder(BorderFactory.createLineBorder(null , 0));
        jp3.setLayout(new GridLayout(0 , 5 , 5 , 5));
        jpp3.add(cartPro);
        jpp3.add(cartNext);
        jf.add(jp6);

        jp4.setPreferredSize(new Dimension(170 , 280));
        jp4.add(new JLabel("支付方式"));
        jp4.setBorder(BorderFactory.createLineBorder(null , 1));
        JButton jb1 = new JButton("微信支付");
        jb1.setPreferredSize(new Dimension(110 , 42));
        jp4.add(jb1);
        JButton jjb1 = new JButton("支付宝支付");
        jjb1.setPreferredSize(new Dimension(150 , 42));
        jp4.add(jjb1);
        jp4.add(jp5);

        jp7.setPreferredSize(new Dimension(930 , 235));
        jp7.setBorder(BorderFactory.createLineBorder(null , 0));
        jp8.setPreferredSize(new Dimension(930 , 155));
        jp8.setLayout(new FlowLayout(FlowLayout.LEFT));
        jp9.setPreferredSize(new Dimension(900 , 35));
        jp8.setBorder(BorderFactory.createLineBorder(null , 1));
        jp9.setBorder(BorderFactory.createLineBorder(null , 0));

        jp8.setLayout(new GridLayout(0 , 5 , 5 , 5));
        jp8.add(new JLabel("商品id"));
        jp8.add(new JLabel("商品名"));
        jp8.add(new JLabel("商品价格"));
        jp8.add(new JLabel("商品库存"));

        getCart(jp3 , user.getId() , cartCurrentPage);

        jp9.add(new JLabel("（￣▽￣）        "
                + "客官不再看看嘛,(｀?ω??)" + "          （￣▽￣）"));
        jp7.add(jp9);
        jp5.setPreferredSize(new Dimension(150 , 150));
        jp5.setBackground(Color.LIGHT_GRAY);
        jp5.setBorder(BorderFactory.createLineBorder(null , 1));
        jp5.add(new JLabel("广告"));
        jp5.add(jsp);

        getGoods(jp3 , jp8 , currentPage , label , user);

        jp7.add(jp8);
        jp7.add(new JLabel("总数为:" + totalCount));
        jp7.add(new JLabel("总页数为:" + totalPage));
        jp7.add(label);
        jp7.add(pro);
        jp7.add(next);

        jf.add(jp7);

        /**
         * 查看购物记录
         */
        jb2.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                try{
                    new UserShopCheck(serviceDao.findAll(user.getId()));
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });
        //支付方式监听
        jb1.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                try{
                    List<UserCart> list = serviceDao.findAllOfCart(user.getId());
                    if(list.size() == 0){
                        JOptionPane.showMessageDialog(null , "您未购买物品哦!" , "提示" , JOptionPane.INFORMATION_MESSAGE);
                    }else{
                        new Pay(user , list , jf);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });
        jjb1.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                try{
                    List<UserCart> list = serviceDao.findAllOfCart(user.getId());
                    if(list.size() == 0){
                        JOptionPane.showMessageDialog(null , "您未购买物品哦!" , "提示" , JOptionPane.INFORMATION_MESSAGE);
                    }else{
                        new Pay(user , list , jf);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });
        //返回主页面监听 jb3
        jb3.addActionListener(e -> {
            try{
                jf.dispose();
                new OnlineStore_Login(user);
            }catch(Exception ex){
                ex.printStackTrace();
            }

        });
        //实现翻页效果 上一页
        pro.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                try{
                    if(currentPage > 1){
                        getGoods(jp3 , jp8 , currentPage - 1 , label , user);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        next.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                try{
                    if(currentPage < totalPage){
                        getGoods(jp3 , jp8 , currentPage + 1 , label , user);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        cartPro.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                try{
                    if(cartCurrentPage > 1){
                        getCart(jp3 , user.getId() , cartCurrentPage - 1);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        cartNext.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                try{
                    if(cartCurrentPage < cartTotalPage){
                        getCart(jp3 , user.getId() , cartCurrentPage + 1);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        //窗口关闭
        jf.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent e){
                jf.dispose();
            }
        });

        jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jf.setVisible(true);
        jf.setResizable(false);//不可调节窗口大小
    }

    /**
     * 获取购物表信息并刷新面板
     * @param jp3
     * @param uid
     * @throws Exception
     */
    public static void getCart(JPanel jp3 , int uid , int curPage) throws Exception{
        jp3.removeAll();
        jp3.repaint();
        //jp3.revalidate();
        PageBean pb = new PageBean();
        pb = serviceDao.findByPage(uid , curPage);
        List<UserCart> list = pb.getList();
        jp3.add(new JLabel("    商品id"));
        jp3.add(new JLabel("    商品名"));
        jp3.add(new JLabel("    商品价格"));
        jp3.add(new JLabel("    数量"));
        jp3.add(new JLabel("    操作"));
        for(UserCart uc : list){
            //System.out.println(uc);
            jp3.add(new JLabel("      " + uc.getGid()));
            jp3.add(new JLabel("  " + uc.getName()));
            jp3.add(new JLabel("      " + uc.getPrice()));
            jp3.add(new JLabel("      " + uc.getQuatity()));
            JButton jb = new JButton("删除");
            jb.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e){
                    try{
                        serviceDao.delete(uc.getGid());
                        getCart(jp3 , uid , curPage);
                    }catch(Exception ex){
                        ex.printStackTrace();
                    }
                }
            });
            jp3.add(jb);

        }
        cartTotalPage = pb.getTotalPage();
        cartCurrentPage = pb.getCurrentPage();
        jp3.revalidate();

    }

    /**
     * 获取商品信息
     * @param jp3
     * @param jp8
     * @param curPage
     * @param label
     * @throws Exception
     */
    public static void getGoods(JPanel jp3 , JPanel jp8 , int curPage , JLabel label , User user) throws Exception{
        jp8.removeAll();
        jp8.repaint();
        PageBean pb = new PageBean();
        UserCart uc;
        pb.setCurrentPage(curPage);
        pb.setPageSize(4); //先设置好每页的条数
        pb = serviceDao.findGoodsByPage(pb);
        List<Good> list = pb.getList();
        jp8.add(new JLabel("商品id"));
        jp8.add(new JLabel("商品名"));
        jp8.add(new JLabel("单价"));
        jp8.add(new JLabel("库存"));
        jp8.add(new JLabel("操作"));
        //System.out.println(pb);
        for(Good good : list){
            //System.out.println(good);
            uc = new UserCart();
            uc.setUid(user.getId());
            uc.setGid(good.getId());
            uc.setName(good.getName());
            uc.setPrice(good.getPrice());
            uc.setQuatity(1);
            jp8.add(new JLabel("  " + good.getId()));
            jp8.add(new JLabel(good.getName()));
            jp8.add(new JLabel(" " + good.getPrice()));
            jp8.add(new JLabel(" " + good.getStock()));
            JButton jb = new JButton("加入购物车");
            UserCart finalUc = uc;
            jb.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e){
                    try{
                        serviceDao.update(finalUc);
                        getCart(jp3 , finalUc.getUid() , cartCurrentPage);
                    }catch(Exception ex){
                        ex.printStackTrace();
                    }
                }
            });
            jp8.add(jb);
        }
        totalPage = pb.getTotalPage();
        currentPage = pb.getCurrentPage();
        totalCount = pb.getTotalCount();
        label.setText("当前页码为:" + currentPage);
        label.setFont(new Font("楷体" , Font.BOLD , 16));
        jp8.revalidate();
    }
}

