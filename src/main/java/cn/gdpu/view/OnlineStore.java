package cn.gdpu.view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.List;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;

import cn.gdpu.domain.Good;
import cn.gdpu.domain.PageBean;
import cn.gdpu.service.ServiceDao;
import cn.gdpu.service.impl.ServiceDaoImpl;
import cn.gdpu.util.DrawPanel;
import cn.gdpu.util.JFrameBackground;


public class OnlineStore{
    //	public OnlineStoreUI(){
    private static ServiceDao serviceDao = new ServiceDaoImpl();

    private static int currentPage = 1;
    private static int totalPage;
    private static int totalCount;

    public static void main(String[] args) throws Exception{

        InitGlobalFont(new Font("楷体" , Font.BOLD , 16));
        //生成需要的面板
        JPanel jp0 = new JPanel();
        JPanel jp1 = new JPanel();
        JPanel jp2 = new JPanel();
        JPanel jp3 = new JPanel();
        JPanel jp4 = new JPanel();
        JPanel jp5 = new JPanel();
        JPanel jp6 = new JPanel();
        JPanel jp7 = new JPanel();
        JPanel jp8 = new JPanel();
        JPanel jp9 = new JPanel();
        JPanel jpp1 = new JPanel();
        JPanel jpp2 = new JPanel();
        JPanel jpp3 = new JPanel();
        JLabel title1 = new JLabel();
        JLabel title2 = new JLabel();
        JLabel title3 = new JLabel();
        JLabel title4 = new JLabel();
        JLabel title5 = new JLabel();
        JLabel title6 = new JLabel();
        JLabel title7 = new JLabel();
        JLabel label = new JLabel();
        JButton pro = new JButton("上一页");
        JButton next = new JButton("下一页");

        JFrame jf = new JFrame("网上商城界面");
        FlowLayout f = new FlowLayout();
        f.setHgap(0);
        f.setVgap(0);
        jf.setBounds(400 , 200 , 1050 , 730);//设计窗口大小
        jf.setLayout(new FlowLayout());//设置窗口布局为流布局
        Toolkit tk = Toolkit.getDefaultToolkit();
        Image i = tk.getImage("src/main/resources/images/titlePicture.jpg");
        jf.setIconImage(i);
        new JFrameBackground(jf , "src/main/resources/images/bgPicture.jpg");

        jp0.setOpaque(false);
        jp1.setOpaque(false);
        jp2.setOpaque(false);
        jp3.setOpaque(false);
        jp4.setOpaque(false);
        jp5.setOpaque(false);
        jp6.setOpaque(false);
        jp7.setOpaque(false);
        jp8.setOpaque(false);
        jp9.setOpaque(false);
        jpp1.setOpaque(false);
        jpp2.setOpaque(false);
        jpp3.setOpaque(false);

        //先new需要的文本域及对应的滚动条
        JTextArea jta = new JTextArea();
        jta.setEditable(false);
        jta.setPreferredSize(new Dimension(165 , 350));//168
        jta.setLineWrap(true);        //激活自动换行功能 
        jta.setWrapStyleWord(true);            // 激活断行不断字功能
        jta.setBackground(Color.LIGHT_GRAY);

        JScrollPane jsp = new JScrollPane(jta);//生成滚动条
        jsp.setPreferredSize(new Dimension(165 , 205));
//		jsp.getVerticalScrollBar().setUnitIncrement(5); //每点击knob一次滚动十个象素
        jsp.getVerticalScrollBar().setBlockIncrement(10); //每点击track一次滚动五象素
        jsp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER); //水平滚动条不显示
        jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED); //自动显示竖直滚动条

//		Toolkit tk = Toolkit.getDefaultToolkit();
//		Image i = tk.getImage("src\\1112.jpg");
        DrawPanel my = new DrawPanel();
        my.setPreferredSize(new Dimension(120 , 100));
        jp1.setPreferredSize(new Dimension(1030 , 110));
        jp1.setBorder(BorderFactory.createLineBorder(null , 1)); //设置边框粗细
        JLabel jl1 = new JLabel("网上购物商城");               //设置标题
//		jl1.setPreferredSize(new Dimension(680,30));
        jl1.setFont(new Font("楷体" , Font.BOLD , 35));//设置标题字体大小
        jp0.add(jl1);
//		jp0.add(new P1anel());
        jp1.add(jp0);
        jp1.add(my);
        jf.add(jp1);

        //中间词条说明
        jp2.setPreferredSize(new Dimension(1030 , 35));
        jp2.setLayout(new FlowLayout(FlowLayout.LEFT));
        title1.setFont(new Font("楷体" , Font.BOLD , 16));
        title1.setText("本站首页 |");
        jp2.add(title1);
        title2.setFont(new Font("楷体" , Font.BOLD , 16));
        title2.setText("最新商品 |");
        jp2.add(title2);
        title3.setFont(new Font("楷体" , Font.BOLD , 16));
        title3.setText("推荐商品 |");
        jp2.add(title3);
        title4.setFont(new Font("楷体" , Font.BOLD , 16));
        title4.setText("热门商品 |");
        jp2.add(title4);
        title5.setFont(new Font("楷体" , Font.BOLD , 16));
        title5.setText("商品分类 |");
        jp2.add(title5);
        title6.setFont(new Font("楷体" , Font.BOLD , 16));
        title6.setText("用户中心 |");
        jp2.add(title6);
        title7.setFont(new Font("楷体" , Font.BOLD , 16));
        title7.setText("订单查询 |		");
        jp2.add(title7);

        JButton jb = new JButton("我的购物车");   //设置相应的按钮
        jb.setPreferredSize(new Dimension(120 , 25));
        jb.setFont(new Font("楷体" , Font.BOLD , 16));
        jb.setBackground(Color.orange);
//		jb.setContentAreaFilled(false);   //设置按钮为透明!
//		jb.setBorderPainted(false);   //设置按钮无边框
//		jb.setMargin(new Insets(0,0,0,0));
//		jb.setBorder(BorderFactory.createRaisedBevelBorder());	
        jp2.add(jb);
        JTextField jt = new JTextField(16);
        jt.setText("请输入商品名");
        jt.setPreferredSize(new Dimension(50 , 25));

        jp2.add(jt);
        JButton jb2 = new JButton("立即查找");
        jb2.setPreferredSize(new Dimension(110 , 25));
        jb2.setFont(new Font("楷体" , Font.BOLD , 16));
        jp2.add(jb2);
        jf.add(jp2);

        //商城公告板块
        jp3.setPreferredSize(new Dimension(1030 , 95));
        jp3.setBorder(BorderFactory.createLineBorder(null , 1));
        JLabel jl2 = new JLabel("商城暂无活动，敬请期待!");
        jl2.setFont(new Font("楷体" , Font.BOLD , 32));
        jp3.add(jl2);
        jf.add(jp3);

        //用户登录及左边框
        jp4.setPreferredSize(new Dimension(175 , 400));
        jp4.setBorder(BorderFactory.createLineBorder(null , 0));
        jp5.setPreferredSize(new Dimension(175 , 100));
        jp6.setPreferredSize(new Dimension(175 , 250));
        jp5.setBorder(BorderFactory.createLineBorder(null , 1));
        jp6.setBorder(BorderFactory.createLineBorder(null , 1));
        jp6.setBackground(Color.lightGray);
        JLabel jl3 = new JLabel("用户登录模块");
        jl3.setFont(new Font(null , Font.BOLD , 20));
        JButton jb4 = new JButton("登录或注册");
        jb4.setPreferredSize(new Dimension(150 , 38));
        JButton jb5 = new JButton("管理员登录");
        jb5.setPreferredSize(new Dimension(150 , 38));


        jpp1.setPreferredSize(new Dimension(160 , 30));
        jpp1.setBackground(Color.lightGray);
        JLabel jl4 = new JLabel("商城公告");
        jta.setText("    欢迎各位用户注册网上购物商城,这里有各种各样的热门商品可供" +
                "各位用户挑选购买。\n    并且本商城会不定期推出新的商品,以回报各位用户!\n"
                + "同时本商城会定期对某些商品进行购买折扣,也会推出相应的商城活动供用户参与!\n"
                + "    更多活动与福利敬请期待!");
        //jta.setFont(new Font("楷体" , Font.BOLD , 16));
        jl4.setFont(new Font(null , Font.BOLD , 20));
        jp6.setLayout(new FlowLayout(FlowLayout.LEFT));
        jpp1.add(jl4);
        jp6.add(jpp1);
        jp6.add(jsp);

        jp5.add(jb4);
        jp5.add(jb5);
        jp4.add(jp5);
        jp4.add(jp6);
        jf.add(jp4);

        //商品区域模块 jp8用来展示商品信息
        jp7.setPreferredSize(new Dimension(850 , 480));
        jp7.setBorder(BorderFactory.createLineBorder(null , 0));
        jp8.setPreferredSize(new Dimension(850 , 280));
        jp8.setLayout(new FlowLayout(FlowLayout.LEFT));
        jp9.setPreferredSize(new Dimension(850 , 105));
        jp9.setLayout(new FlowLayout(FlowLayout.CENTER));
        jp8.setBorder(BorderFactory.createLineBorder(null , 1));
        jp9.setBorder(BorderFactory.createLineBorder(null , 1));
        JLabel jl5 = new JLabel("搜索结果如下");
        jpp2.setPreferredSize(new Dimension(830,20));
        jpp2.add(jl5);
        jpp3.setPreferredSize(new Dimension(840,70));
        jpp3.setLayout(new GridLayout(0,5,5,5));
        //jpp3.setBorder(BorderFactory.createLineBorder(null,1));
        jp9.add(jpp2);
        jp9.add(jpp3);
        jp8.setLayout(new GridLayout(0 , 5,5,5));
        getGoods(jp8 , currentPage,label);

        jp7.add(jp8);
        jp7.add(new JLabel("总数为:" + totalCount));
        jp7.add(new JLabel("总页数为:" + totalPage));
        jp7.add(label);
        jp7.add(pro);
        jp7.add(next);
        jp7.add(jp9);
        jf.add(jp7);
        
        jt.addMouseListener(new MouseAdapter(){
            /**
             * {@inheritDoc}
             * @param e
             */
            @Override
            public void mouseEntered(MouseEvent e){
                jt.setText("");
                jt.requestFocus();
            }
        });
        //查找按钮监听
        jb2.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                try{
                    findGoods(jpp3,jt);
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });
        //设置登录注册监听
        jb4.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e){
                // TODO Auto-generated method stub
                new LoginAndRegister();
                jf.dispose();
            }
        });
        //设置登录
        jb5.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new ManagerLogin();
                jf.dispose();
            }
        });
        //设置我的购物车的监听事件
        jb.addActionListener(e -> JOptionPane.showMessageDialog(null , "请先登录!" , "提示" , JOptionPane.ERROR_MESSAGE));
        //实现翻页效果 上一页
        pro.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                try{
                    if(currentPage > 1){
                        getGoods(jp8 , currentPage - 1 ,label);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        next.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                try{
                    if(currentPage < totalPage){
                        getGoods(jp8 , currentPage + 1,label);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        //窗口关闭
        jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jf.setVisible(true);
        //jf.setResizable(true);//不可调节窗口大小
    }

    /**
     * 获取商品信息
     * @param jp8
     * @param curPage
     * @param label
     * @throws Exception
     */
    private static void getGoods(JPanel jp8 , int curPage,JLabel label) throws Exception{
        jp8.removeAll();
        jp8.repaint();
        PageBean pb = new PageBean();
        pb.setCurrentPage(curPage);
        pb.setPageSize(6); //设置每页的条数
        pb = serviceDao.findGoodsByPage(pb);
        List<Good> list = pb.getList();
        jp8.add(new JLabel("商品id"));
        jp8.add(new JLabel("商品名"));
        jp8.add(new JLabel("单价"));
        jp8.add(new JLabel("库存"));
        jp8.add(new JLabel("操作"));
        //System.out.println(pb);
        for(Good good : list){
            //System.out.println(good);
            jp8.add(new JLabel("  "+ good.getId()));
            jp8.add(new JLabel(good.getName()));
            jp8.add(new JLabel(" "+ good.getPrice()));
            jp8.add(new JLabel(" "+ good.getStock()));
            JButton jb = new JButton("加入购物车");
            jb.addActionListener(e -> {
                try{
                    JOptionPane.showMessageDialog(null , "请先登录!" , "提示" , JOptionPane.ERROR_MESSAGE);
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            });
            jp8.add(jb);
        }
        totalPage = pb.getTotalPage();
        currentPage = pb.getCurrentPage();
        totalCount = pb.getTotalCount();
        label.setText("当前页码为:" + currentPage);
        label.setFont(new Font("楷体" , Font.BOLD , 16));
        jp8.revalidate();
    }

    /**
     * 查找操作
     * @param jpp3
     * @param jt
     * @throws Exception
     */
    private static void findGoods(JPanel jpp3,JTextField jt) throws Exception{
        jpp3.removeAll();
        jpp3.repaint();
        String text = jt.getText().trim();
        List<Good> list = serviceDao.findGoodByName(text);
        jpp3.add(new JLabel("商品id"));
        jpp3.add(new JLabel("商品名"));
        jpp3.add(new JLabel("单价"));
        jpp3.add(new JLabel("库存"));
        jpp3.add(new JLabel("操作"));
        if(list != null){
            for(Good good : list){
                //System.out.println(good);
                jpp3.add(new JLabel("  " + good.getId()));
                jpp3.add(new JLabel(good.getName()));
                jpp3.add(new JLabel(" " + good.getPrice()));
                jpp3.add(new JLabel(" " + good.getStock()));
                JButton jb = new JButton("加入购物车");
                jb.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e){
                        try{
                            JOptionPane.showMessageDialog(null , "请先登录!" , "提示" , JOptionPane.ERROR_MESSAGE);
                        }catch(Exception ex){
                            ex.printStackTrace();
                        }
                    }
                });
                jpp3.add(jb);
            }
        }
        jpp3.revalidate();
    }

    /**
     * 统一设置字体，父界面设置之后，所有由父界面进入的子界面都不需要再次设置字体
     */
    public static void InitGlobalFont(Font font){
        FontUIResource fontRes = new FontUIResource(font);
        for(Enumeration<Object> keys = UIManager.getDefaults().keys() ;
            keys.hasMoreElements() ; ){
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if(value instanceof FontUIResource){
                UIManager.put(key , fontRes);
            }
        }
    }

}