package cn.gdpu.view;

import cn.gdpu.domain.User;
import cn.gdpu.domain.UserCart;
import cn.gdpu.domain.UserShop;
import cn.gdpu.service.ServiceDao;
import cn.gdpu.service.impl.ServiceDaoImpl;
import cn.gdpu.util.CheckCode;
import cn.gdpu.util.JFrameBackground;
import cn.gdpu.util.UuidUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Date;
import java.util.List;

public class Pay{

    private int count = 3;//密码输入次数
    private Integer a;
    private static JFrame JF1 = null;
    //	JTextField text1 = new JTextField(10);
    private JPasswordField text2 = new JPasswordField(10);
    private JTextField text4 = new JTextField(20);
    private JTextField text5 = new JTextField(5);
    private String checkCode = CheckCode.getNum();
	private static JLabel checkcode = new JLabel();
	private static JLabel label = new JLabel();
    private static ServiceDao serviceDao = new ServiceDaoImpl();
    private static int cost;
    public Pay(User user, List<UserCart> list,JFrame jf){
    	
        cost = 0;
        OnlineStore.InitGlobalFont(new Font("楷体" , Font.BOLD , 16));
        JF1 = new JFrame("支付界面");
        Toolkit tk = Toolkit.getDefaultToolkit();
        Image i = tk.getImage("src/main/resources/images/Pay.jpg");
        JF1.setIconImage(i);
        //生成背景图片

        JF1.setBounds(600 , 400 , 300 , 280);
        new JFrameBackground(JF1 , "src/main/resources/images/bgPicture.jpg");
        JF1.setLayout(new FlowLayout(FlowLayout.CENTER)); //设置外框架为居中对齐
        JPanel jp = new JPanel(new FlowLayout(FlowLayout.CENTER));//设置内框架jp为居中对齐
        //新建3个面板
        JPanel jp0 = new JPanel();
        JPanel jp1 = new JPanel();
        JPanel jp2 = new JPanel();
        JPanel jp3 = new JPanel();
        //面板透明化
        jp0.setOpaque(false);
        jp1.setOpaque(false);
        jp2.setOpaque(false);
        jp3.setOpaque(false);
        //设置对应的标签
//		JLabel jl1 = new JLabel("用户名");
        
        JLabel jl2 = new JLabel("密  码");
        JLabel jl3 = new JLabel("验证码");
        
        checkcode.setIcon(new ImageIcon(CheckCode.checkCode(checkCode)));

        jp0.setPreferredSize(new Dimension(280 , 30));
        jp0.add(label);
        for(UserCart uc : list){
            cost += uc.getPrice()*uc.getQuatity();
        }
        label.setText("应付金额:"+cost);
        label.setFont(new Font("楷体",Font.BOLD,16));
        text4.setEditable(false);
        JButton jb1 = new JButton("确认支付");
        jb1.setPreferredSize(new Dimension(150 , 30));
        jb1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                // TODO Auto-generated method stub
				try{
					pay(user,list,jf);
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
        });

        //标签与文本域加入面板之中
//		jp1.add(jl1);
//		jp1.add(text1);
        jp2.add(jl2);
        jp2.add(text2);
        jp3.add(jl3);
        jp3.add(checkcode);

        jp.add(jb1);
//		jp.add(jb2);
//		jp.add(jb3);
        //全部面板加入JF窗口中
        JF1.add(jp0);
        JF1.add(jp1);
        JF1.add(jp2);
        JF1.add(jp3);
        JF1.add(text5);
        JF1.add(jp);
        JF1.add(text4);
        //生成验证码 or 更换验证码
        checkcode.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                checkCode = CheckCode.getNum();
				checkcode.setIcon(new ImageIcon(CheckCode.checkCode(checkCode)));
            }
        });
        //设置窗口关闭监听器
        JF1.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                JF1.dispose();
            }
        });
        JF1.setVisible(true);
        JF1.setResizable(false);//不可调节窗口大小
    }

    /**
     * 支付验证
     * @param user
     * @param list
     * @throws Exception
     */
    public void pay(User user, List<UserCart> list,JFrame jf) throws Exception{
        //账号密码及验证码的输入
        String password = text2.getText().trim();
        String Captcha = text5.getText().trim();
        if(count == 0){
            JOptionPane.showMessageDialog(null , "账户已被锁定,请与管理员联系!" , "警告" , JOptionPane.ERROR_MESSAGE);
            text4.setText("账户已被锁定!");
        }else{
            if(password.equals(""))
                JOptionPane.showMessageDialog(null , "请输入密码!" , "提示" , JOptionPane.INFORMATION_MESSAGE);
            else if(judgeCheckCode(checkCode,Captcha) && password.equals(user.getPassword())){
            	
                JOptionPane.showMessageDialog(null , "支付成功!" , "提示" , JOptionPane.INFORMATION_MESSAGE);
				
                //封装UserShop
				UserShop us = new UserShop();
				us.setUid(user.getId());
				us.setList(UuidUtil.getUuid());
				us.setCost(cost);
				us.setDate(new Date());
				serviceDao.insertUserShop(us,list);	
				JF1.dispose();
                jf.dispose();
				new ShoppingCart(user);
            }else if(!judgeCheckCode(checkCode,Captcha)){
                JOptionPane.showMessageDialog(null , "验证码错误!请重新输入。" , "提示" , JOptionPane.ERROR_MESSAGE);
				text5.setText("");       //对应的要输入验证码的区域设为空
				text5.requestFocus();    //调整注意位置
				checkCode = CheckCode.getNum();
				checkcode.setIcon(new ImageIcon(CheckCode.checkCode(checkCode)));
            }else{
                JOptionPane.showMessageDialog(null , "密码错误" , "提示" , JOptionPane.ERROR_MESSAGE);
                text2.setText("");
                text2.requestFocus();
                text5.setText("");       //对应的要输入验证码的区域设为空
                text4.setText("还有" + --count + "输入机会!");
                
                
                if(count == 0){
                    JOptionPane.showMessageDialog(null , "账户已被锁定,请与管理员联系!" , "警告" , JOptionPane.ERROR_MESSAGE);
                    text4.setText("账户已被锁定!");
                }
            }
        }
    }

    /**
     * 验证码验证
     * @param checkCode
     * @param value
     * @return
     */
	public static boolean judgeCheckCode(String checkCode , String value){
		return checkCode.equalsIgnoreCase(value);
	}
	
    //public static void main(String[] args) throws Exception{
	//	User user = new User();
	//	user.setId(1);
	//	user.setName("hellott");
	//	user.setPassword("123456");
	//	List<UserCart> list = serviceDao.findAllOfCart(user.getId());
	//	new Pay_UI(user,list);
    //}
}
