package cn.gdpu.view;

import cn.gdpu.domain.Good;
import cn.gdpu.domain.Manager;
import cn.gdpu.domain.PageBean;
import cn.gdpu.domain.User;
import cn.gdpu.service.ServiceDao;
import cn.gdpu.service.impl.ServiceDaoImpl;
import cn.gdpu.util.JFrameBackground;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class OnlineStore_manager{

    private static JButton jb0 = new JButton("插入用户信息");
    private static JButton jb1 = new JButton("插入商品数据");
    private static JButton jb2 = new JButton("查看商品数据");
    private static JButton jb3 = new JButton("查看用户信息");
    private static JButton jb4 = new JButton("退出后台管理");
    private static JLabel label = new JLabel();

    private static int currentPage = 1;
    private static int totalPage;
    private static int totalCount;
    private static int userCurrentPage = 1;
    private static int userTotalPage;
    private static int userTotalCount;

    private static ServiceDao serviceDao = new ServiceDaoImpl();
    //private static Manager manager = new Manager();

    public OnlineStore_manager(Manager manager){
    //public static void main(String[] args){
        //manager.setId(1);
        //manager.setName("hellott");
        OnlineStore.InitGlobalFont(new Font("楷体" , Font.BOLD , 16));
        JFrame jf = new JFrame("管理员界面");
        jf.setBounds(400 , 200 , 900 , 700);
        jf.setLayout(new FlowLayout());//设置窗口布局为流布局
        Toolkit tk = Toolkit.getDefaultToolkit();
        Image i = tk.getImage("src/main/resources/images/user.jpg");
        jf.setIconImage(i);
        new JFrameBackground(jf , "src/main/resources/images/bgPicture.jpg");
        JPanel jp0 = new JPanel();
        JPanel jp1 = new JPanel();
        JPanel jp2 = new JPanel();
        JPanel jp3 = new JPanel();
        JPanel jp4 = new JPanel();

        jp0.setOpaque(false);
        jp1.setOpaque(false);
        jp2.setOpaque(false);
        jp3.setOpaque(false);
        jp4.setOpaque(false);
        jb0.setFont(new Font("楷体" , Font.BOLD , 16));
        jb1.setFont(new Font("楷体" , Font.BOLD , 16));
        jb2.setFont(new Font("楷体" , Font.BOLD , 16));
        jb3.setFont(new Font("楷体" , Font.BOLD , 16));
        jb4.setFont(new Font("楷体" , Font.BOLD , 16));

        jp0.setPreferredSize(new Dimension(880 , 150));
        jp0.setBorder(BorderFactory.createLineBorder(null , 1));

        jp1.add(new JLabel("欢迎您," + manager.getName()));
        jp1.add(jb1);
        jp1.add(jb2);
        jp1.add(jb0);
        jp1.add(jb3);
        jp1.add(jb4);
        jp1.setPreferredSize(new Dimension(150 , 655));
        jp1.setBorder(BorderFactory.createLineBorder(null , 1));
        jp2.setPreferredSize(new Dimension(725 , 655));
        jp2.setBorder(BorderFactory.createLineBorder(null , 1));

        //jf.add(jp0);
        jf.add(jp1);
        jf.add(jp2);

        /**
         * 插入商品
         */
        jb1.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                getInsert(jp2);
            }
        });

        /**
         * 插入用户
         */
        jb0.addActionListener(e -> getInsertUser(jp2));

        /**
         * 查看商品数据
         */
        jb2.addActionListener(e -> {
            try{
                getGoods(jp2 , currentPage , label);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        });

        /**
         * 查看用户信息
         */
        jb3.addActionListener(e -> {
            try{
                getUsers(jp2,userCurrentPage,label);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        });

        /**
         * 退出后台管理
         */
        jb4.addActionListener(e -> {
            jf.dispose();
            try{
                OnlineStore.main(null);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        });

        jf.setVisible(true);

        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * 插入新用户
     * @param jp2
     */
    private static void getInsertUser(JPanel jp2){
        jp2.removeAll();
        jp2.repaint();
        jp2.add(new JLabel("插入用户数据"));
        JPanel jPanel = new JPanel();
        jPanel.setPreferredSize(new Dimension(708 , 100));
        jPanel.setBorder(BorderFactory.createLineBorder(null , 1));
        jPanel.setLayout(new GridLayout(0 , 4 , 5 , 5));
        jPanel.add(new JLabel("用户id"));
        jPanel.add(new JLabel("用户名"));
        jPanel.add(new JLabel("用户密码"));
        jPanel.add(new JLabel("操作"));
        jPanel.add(new JLabel(""));
        JTextField jt1 = new JTextField(10);
        JTextField jt2 = new JTextField(10);
        JButton ins = new JButton("插入数据");
        jPanel.add(jt1);
        jPanel.add(jt2);
        jPanel.add(ins);
        ins.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                try{
                    User user = new User();
                    user.setId(null);
                    if("".equals(jt1.getText().trim()) || "".equals(jt2.getText().trim())){
                        System.out.println("请输入正常数据!谢谢配合。");
                    }else{
                        user.setName(jt1.getText().trim());
                        user.setPassword(jt2.getText().trim());
                        serviceDao.insert(user);
                        System.out.println("插入用户成功!");
                        JOptionPane.showMessageDialog(null , "插入成功!" , "提示" , JOptionPane.INFORMATION_MESSAGE);
                        jt1.setText("");
                        jt2.setText("");
                    }
                }catch(Exception ex){
                    System.out.println("请输入正常数据!谢谢配合。");
                }
            }
        });
        jp2.add(jPanel);
        jp2.revalidate();
    }

    /**
     * 查看用户信息
     * @param jp2
     * @param curPage
     * @param label
     */
    private static void getUsers(JPanel jp2 , int curPage , JLabel label) throws Exception{
        jp2.removeAll();
        jp2.repaint();
        JPanel jPanel = new JPanel();
        jPanel.setPreferredSize(new Dimension(708 , 610));
        jPanel.setBorder(BorderFactory.createLineBorder(null , 1));
        jPanel.setLayout(new GridLayout(0 , 6 , 5 , 5));
        jPanel.add(new JLabel("用户id"));
        jPanel.add(new JLabel("用户名"));
        jPanel.add(new JLabel("用户密码"));
        jPanel.add(new JLabel("操作"));
        jPanel.add(new JLabel("操作"));
        jPanel.add(new JLabel("操作"));
        PageBean pb = new PageBean();
        pb.setCurrentPage(curPage);
        //pb.setPageSize(10);
        pb = serviceDao.findUsersByPage(curPage);
        userTotalPage = pb.getTotalPage();
        userCurrentPage = pb.getCurrentPage();
        userTotalCount = pb.getTotalCount();
        List<User> list = pb.getList();
        for(User user:list){
            jPanel.add(new JLabel("  " + user.getId()));
            JTextField jt1 = new JTextField(user.getName());
            JTextField jt2 = new JTextField(user.getPassword());
            jPanel.add(jt1);
            jPanel.add(jt2);
            JButton jb = new JButton("更新");
            JButton jb1 = new JButton("删除");
            JButton jb2 = new JButton("查看记录");
            jb.addActionListener(e -> {
                try{
                    User user1 = new User();
                    user1.setId(user1.getId());
                    if("".equals(jt1.getText().trim()) || "".equals(jt2.getText().trim())){
                        System.out.println("请插入正常数据!谢谢配合。");
                    }else{
                        user1.setName(jt1.getText().trim());
                        user1.setPassword(jt2.getText().trim());
                        //System.out.println(good1);
                        if(!(user.getName().equals(user1.getName()) && user.getPassword().equals(user1.getPassword()))){
                            serviceDao.updateUser(user1);
                            getUsers(jp2 , 1 , label);
                            System.out.println("更新用户数据成功!");
                        }else{
                            System.out.println("数据没变化,不更新!");
                        }
                    }
                }catch(Exception ex){
                    System.out.println("请插入正常数据!谢谢配合。");
                }
            });
            jb1.addActionListener(e -> {
                try{
                    serviceDao.deleteUserById(user.getId());
                    getGoods(jp2 , 1 , label);
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            });
            jb2.addActionListener(e -> {
                try{
                    new UserShopCheck(serviceDao.findAll(user.getId()));
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            });
            jPanel.add(jb);
            jPanel.add(jb1);
            jPanel.add(jb2);
        }
        JButton pro = new JButton("上一页");
        JButton next = new JButton("下一页");
        pro.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                try{
                    if(userCurrentPage > 1)
                        getUsers(jp2 , userCurrentPage - 1 , label);
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });
        next.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                try{
                    if(userCurrentPage < userTotalPage){
                        getUsers(jp2 , userCurrentPage + 1 , label);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });
        jp2.add(jPanel);
        jp2.add(new JLabel("总数为:" + userTotalCount));
        jp2.add(new JLabel("总页数为:" + userTotalPage));
        label.setText("当前页码为:" + userCurrentPage);
        label.setFont(new Font("楷体" , Font.BOLD , 16));
        jp2.add(label);
        jp2.add(pro);
        jp2.add(next);
        jp2.revalidate();
    }

    /**
     * 对商品表插入数据
     * @param jp2
     */
    private static void getInsert(JPanel jp2){
        jp2.removeAll();
        jp2.repaint();
        jp2.add(new JLabel("插入商品数据"));
        JPanel jPanel = new JPanel();
        jPanel.setPreferredSize(new Dimension(708 , 100));
        jPanel.setBorder(BorderFactory.createLineBorder(null , 1));
        jPanel.setLayout(new GridLayout(0 , 5 , 5 , 5));
        jPanel.add(new JLabel("商品id"));
        jPanel.add(new JLabel("商品名"));
        jPanel.add(new JLabel("单价"));
        jPanel.add(new JLabel("库存"));
        jPanel.add(new JLabel("操作"));
        jPanel.add(new JLabel(""));
        JTextField jt1 = new JTextField(10);
        JTextField jt2 = new JTextField(10);
        JTextField jt3 = new JTextField(10);
        JButton ins = new JButton("插入数据");
        jPanel.add(jt1);
        jPanel.add(jt2);
        jPanel.add(jt3);
        jPanel.add(ins);
        ins.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                try{
                    Good good = new Good();
                    good.setUid(null);
                    if("".equals(jt1.getText().trim()) || "".equals(jt2.getText().trim()) || "".equals(jt3.getText().trim())){
                        System.out.println("请输入正常数据!谢谢配合。");
                    }else{
                        good.setName(jt1.getText().trim());
                        good.setPrice(Integer.parseInt(jt2.getText().trim()));
                        good.setStock(Integer.parseInt(jt3.getText().trim()));
                        serviceDao.insertGood(good);
                        System.out.println("插入商品成功!");
                        JOptionPane.showMessageDialog(null , "插入成功!" , "提示" , JOptionPane.INFORMATION_MESSAGE);
                        jt1.setText("");
                        jt2.setText("");
                        jt3.setText("");
                    }
                }catch(Exception ex){
                    System.out.println("请输入正常数据!谢谢配合。");
                }
            }
        });

        jp2.add(jPanel);
        jp2.revalidate();
    }

    /**
     * 对商品表的查看更新操作
     * @param jp2
     * @param curPage
     * @param label
     * @throws Exception
     */
    private static void getGoods(JPanel jp2 , int curPage , JLabel label) throws Exception{
        jp2.removeAll();
        jp2.repaint();
        JPanel jPanel = new JPanel();
        jPanel.setPreferredSize(new Dimension(708 , 610));
        jPanel.setBorder(BorderFactory.createLineBorder(null , 1));
        jPanel.setLayout(new GridLayout(0 , 6 , 5 , 5));
        PageBean pb = new PageBean();
        pb.setCurrentPage(curPage);
        pb.setPageSize(10);
        pb = serviceDao.findGoodsByPage(pb);
        totalPage = pb.getTotalPage();
        currentPage = pb.getCurrentPage();
        totalCount = pb.getTotalCount();
        List<Good> list = pb.getList();
        jPanel.add(new JLabel("用户id"));
        jPanel.add(new JLabel("用户名"));
        jPanel.add(new JLabel("单价"));
        jPanel.add(new JLabel("库存"));
        jPanel.add(new JLabel("操作"));
        jPanel.add(new JLabel("操作"));
        for(Good good : list){
            //System.out.println(good);
            jPanel.add(new JLabel("  " + good.getId()));
            JTextField jt1 = new JTextField(good.getName());
            JTextField jt2 = new JTextField(good.getPrice().toString());
            JTextField jt3 = new JTextField(good.getStock().toString());
            jPanel.add(jt1);
            jPanel.add(jt2);
            jPanel.add(jt3);
            JButton jb = new JButton("更新");
            JButton jb1 = new JButton("删除");
            jb.addActionListener(e -> {
                try{
                    Good good1 = new Good();
                    good1.setUid(good.getId());
                    good1.setName(jt1.getText().trim());
                    if("".equals(jt1.getText().trim()) || "".equals(jt2.getText().trim()) || "".equals(jt3.getText().trim())){
                        System.out.println("请插入正常数据!谢谢配合。");
                    }else{
                        good1.setPrice(Integer.parseInt(jt2.getText().trim()));
                        good1.setStock(Integer.parseInt(jt3.getText().trim()));
                        //System.out.println(good1);
                        if(!(good.getName().equals(good1.getName()) && good.getPrice() == good1.getPrice() && good.getStock() == good1.getStock())){
                            serviceDao.updateGood(good1);
                            getGoods(jp2 , 1 , label);
                            System.out.println("更新商品数据成功!");
                        }else{
                            System.out.println("数据没变化,不更新!");
                        }
                    }
                }catch(Exception ex){
                    System.out.println("请插入正常数据!谢谢配合。");
                }
            });
            jb1.addActionListener(e -> {
                try{
                    serviceDao.deleteGoodById(good.getId());
                    getGoods(jp2 , 1 , label);
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            });
            jPanel.add(jb);
            jPanel.add(jb1);
        }

        JButton pro = new JButton("上一页");
        JButton next = new JButton("下一页");
        pro.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                try{
                    if(currentPage > 1)
                        getGoods(jp2 , currentPage - 1 , label);
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });
        next.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                try{
                    if(currentPage < totalPage){
                        getGoods(jp2 , currentPage + 1 , label);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });
        jp2.add(jPanel);
        jp2.add(new JLabel("总数为:" + totalCount));
        jp2.add(new JLabel("总页数为:" + totalPage));
        label.setText("当前页码为:" + currentPage);
        label.setFont(new Font("楷体" , Font.BOLD , 16));
        jp2.add(label);
        jp2.add(pro);
        jp2.add(next);
        jp2.revalidate();
    }


}
