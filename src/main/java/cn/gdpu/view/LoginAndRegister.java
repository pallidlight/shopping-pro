package cn.gdpu.view;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import cn.gdpu.domain.User;
import cn.gdpu.service.ServiceDao;
import cn.gdpu.service.impl.ServiceDaoImpl;
import cn.gdpu.util.CheckCode;
import cn.gdpu.util.JFrameBackground;

public class LoginAndRegister{
    private int count = 2;
    private String checkCode; //验证码的值
    private ServiceDao serviceDao = new ServiceDaoImpl();
    //设置对应的文本域大小
    private JFrame JF1 = null;
    private JTextField text1 = new JTextField(12);
    private JPasswordField text2 = new JPasswordField(12);
    private JTextField text4 = new JTextField(20);
    private JTextField text5 = new JTextField(6);
    private JLabel label = new JLabel();

    public LoginAndRegister(){

        JF1 = new JFrame("注册与登录界面");
        Toolkit tk = Toolkit.getDefaultToolkit();
        Image i = tk.getImage("src/main/resources/images/user.jpg");
        JF1.setIconImage(i);


        JF1.setBounds(600 , 400 , 320 , 300);
        //生成背景图片
        new JFrameBackground(JF1 , "src/main/resources/images/bgPicture.jpg");
        JF1.setLayout(new FlowLayout(FlowLayout.CENTER)); //设置外框架为居中对齐
        JPanel jp = new JPanel(new FlowLayout(FlowLayout.CENTER));//设置内框架jp为居中对齐
        //新建3个面板
        JPanel jp1 = new JPanel();
        JPanel jp2 = new JPanel();
        JPanel jp3 = new JPanel();
        //面板透明化
        jp1.setOpaque(false);
        jp2.setOpaque(false);
        jp3.setOpaque(false);
        //设置对应的标签
        JLabel jl1 = new JLabel("用户名");
        jl1.setFont(new Font("楷体" , Font.BOLD , 16));
        JLabel jl2 = new JLabel("密  码");
        jl2.setFont(new Font("楷体" , Font.BOLD , 16));
        JLabel jl3 = new JLabel("验证码");
        jl3.setFont(new Font("楷体" , Font.BOLD , 16));


        text4.setEditable(false);


        //标签与文本域加入面板之中
        jp1.add(jl1);
        jp1.add(text1);
        jp1.setFont(new Font("楷体" , Font.BOLD , 16));
        jp2.add(jl2);
        jp2.setFont(new Font("楷体" , Font.BOLD , 16));
        jp2.add(text2);
        jp3.add(jl3);
        jp3.setFont(new Font("楷体" , Font.BOLD , 16));

        //添加验证码的图
        //获取随机值
        checkCode = CheckCode.getNum();
        label.setIcon(new ImageIcon(CheckCode.checkCode(checkCode)));
        jp3.add(label);
        label.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                checkCode = CheckCode.getNum();
                label.setIcon(new ImageIcon(CheckCode.checkCode(checkCode)));
            }
        });

        //新建三个按钮,并设置监听 
        /**
         * 登录监听
         */
        JButton jb1 = new JButton("登录");
        jb1.setFont(new Font("楷体" , Font.BOLD , 16));
        jb1.setPreferredSize(new Dimension(70 , 30));
        jb1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                // TODO Auto-generated method stub
                try{
                    login();
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });
        
        /**
         * 注册监听
         */
        JButton jb2 = new JButton("注册");
        jb2.setFont(new Font("楷体" , Font.BOLD , 16));
        jb2.setPreferredSize(new Dimension(70 , 30));
        jb2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                JF1.dispose();
                new UserRegister();
            }
        });
        
        /**
         *退出监听
         */
        JButton jb3 = new JButton("退出");
        jb3.setFont(new Font("楷体" , Font.BOLD , 16));
        jb3.setPreferredSize(new Dimension(70 , 30));
        jb3.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                // TODO Auto-generated method stub
                try{
                    JF1.dispose();
                    OnlineStore.main(null);
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });
        //按钮加入jp面板之中
        jp.add(jb1);
        jp.add(jb2);
        jp.add(jb3);
        //全部面板加入JF窗口中
        JF1.add(jp1);
        JF1.add(jp2);
        JF1.add(jp3);
        JF1.add(text5);
        JF1.add(jp);

        JF1.add(text4);
        //设置窗口关闭监听器
        JF1.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                JF1.dispose();
                //OnlineStoreUI.main(null);
            }
        });
        JF1.setVisible(true);
        JF1.setResizable(false);//不可调节窗口大小
    }

    /**
     * 登录验证
     * @throws Exception
     */
    public void login() throws Exception{
        // TODO Auto-generated method stub
        //账号密码及验证码的输入
        final String username = text1.getText().trim();
        final String password = text2.getText().trim();
        final String captcha = text5.getText().trim();
//		int aa = (int) a;

//		System.out.println(Captcha+" "+a);
        if(count == 0){
            JOptionPane.showMessageDialog(null , "账户已被锁定,请与管理员联系!" , "警告" , JOptionPane.ERROR_MESSAGE);
            text4.setText("账户已被锁定!");
        }else if("".equals(username))
            JOptionPane.showMessageDialog(null , "用户名不能为空!" , "提示" , JOptionPane.INFORMATION_MESSAGE);
        else if("".equals(password))
            JOptionPane.showMessageDialog(null , "密码不能为空!" , "提示" , JOptionPane.INFORMATION_MESSAGE);
        else{
            User user = serviceDao.findOne(username , password);
            Boolean flag = user != null;
            if(flag && judgeCheckCode(checkCode , captcha)){
                JOptionPane.showMessageDialog(null , "登录成功系统!" , "提示" , JOptionPane.INFORMATION_MESSAGE);
                JF1.dispose();
                new OnlineStore_Login(user);
            }else if(flag && !judgeCheckCode(checkCode , captcha)){
                JOptionPane.showMessageDialog(null , "验证码错误!请重新输入。" , "提示" , JOptionPane.INFORMATION_MESSAGE);
                checkCode = CheckCode.getNum();
                label.setIcon(new ImageIcon(CheckCode.checkCode(checkCode)));
                text5.setText("");       //对应的要输入验证码的区域设为空
                text5.requestFocus();    //调整注意位置
            }else{
                if(count == 0)
                    JOptionPane.showMessageDialog(null , "账户已被锁定,请与管理员联系!" , "警告" , JOptionPane.ERROR_MESSAGE);
                text1.setText("");
                text2.setText("");
                if(count == 0)
                    JOptionPane.showMessageDialog(null , "账户已被锁定,请与管理员联系!" , "警告" , JOptionPane.ERROR_MESSAGE);
                else{
                    JOptionPane.showMessageDialog(null , "账号或密码错误" , "提示" , JOptionPane.ERROR_MESSAGE);
                    text4.setText("还有" + count-- + "输入机会!");
                }
                text1.requestFocus();
            }
        }

    }

    /**
     * 验证码验证
     * @param checkCode
     * @param value
     * @return
     */
    public static boolean judgeCheckCode(String checkCode , String value){
        return checkCode.equalsIgnoreCase(value);
    }

    public static void main(String[] args){
        new LoginAndRegister();
    }

}
