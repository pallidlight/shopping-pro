package cn.gdpu.util;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class CheckCode{

    public CheckCode(){}
    
    public static Image checkCode(String num) {
        //绘制验证码
        BufferedImage image = new BufferedImage(80,30,BufferedImage.TYPE_INT_RGB);
        //得到画笔
        Graphics graphics = image.getGraphics();
        graphics.fillRect(0,0,80,30);
        //绘制干扰线条
        for(int i=0;i<60;i++){
            Random ran = new Random();
            int xBegin = ran.nextInt(80);
            int yBegin = ran.nextInt(30);

            int xEnd = ran.nextInt(xBegin + 10);
            int yEnd = ran.nextInt(yBegin + 10);
            //得到颜色
            graphics.setColor(getColor());
            //绘制线条
            graphics.drawLine(xBegin,yBegin,xEnd,yEnd);
        }
        graphics.setFont(new Font("seif",Font.BOLD,20));

        //绘制验证码
        //验证码 得搞一个固定颜色 不然会混淆
        graphics.setColor(Color.BLACK);
        String checkCode = num; //把每一位提出来 然后加空格 2 3 4 8
        StringBuffer sb = new StringBuffer();
        for (int i=0;i<checkCode.length();i++){
            sb.append(checkCode.charAt(i)+" "); //验证码的每一位数字
        }
        graphics.drawString(sb.toString(),5,20); //x y 验证码的位置 不过最好就每位字符进行随机位置
        
        //产生真实图片
        return image;

    }
    
        public static Color getColor(){
            //随机产生颜色值
            Random ran = new Random(); //
            int r = ran.nextInt(256);//产生 0-255随机数
            int g = ran.nextInt(256);
            int b = ran.nextInt(256);
            return new Color(r,g,b);//red green blue
        }

        //产生验证码值
        public static String getNum(){
            String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghigklmnopqrstuvwxyz0123456789";
            StringBuilder sb = new StringBuilder();
            Random ran = new Random();
            for (int i = 1; i <= 4; i++){
                int index = ran.nextInt(str.length());
                //获取字符
                char ch = str.charAt(index);//随机字符
                sb.append(ch);
            }
            return sb.toString();
        }
}
