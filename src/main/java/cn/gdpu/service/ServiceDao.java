package cn.gdpu.service;

import cn.gdpu.domain.*;

import java.io.IOException;
import java.util.List;

public interface ServiceDao{

    /**
     * 查找单用户
     * @param name
     * @param password
     * @return
     * @throws Exception
     */
    User findOne(String name , String password) throws Exception;

    /**
     * 插入用户信息
     * @param user
     * @return
     * @throws Exception
     */
    int insert(User user) throws Exception;

    /**
     * 根据uid查找用户密码
     */
    int findPasswordById(int id);

    /**
     * 查询所有商品信息
     * @return
     */
    PageBean findGoodsByPage(PageBean pb) throws Exception;

    /**
     *查询购物表中该商品是否存在 
     */
    int checkUserCart(UserCart uc);
    
    /**
     *更新购物车的东西 
     */
    void update(UserCart uc) throws Exception;
    
    /**
     *查询用户购物车所存在的东西
     */
    List<UserCart> findAllOfCart(int uid) throws Exception;

    /**
     * 分页查询
     * @param uid
     * @param curPage
     * @return
     */
    PageBean findByPage(int uid,int curPage) throws Exception;
    
    /**
     * 删除购物表的东西
     */
    void delete(int gid) throws Exception;

    /**
     * 根据名字查询商品
     */
    List<Good> findGoodByName(String name) throws Exception;

    /**
     * 插入购物记录
     */
    void insertUserShop(UserShop us,List<UserCart> uc) throws Exception;

    /**
     * 查看购物记录
     */
    List<UserShop> findAll(int uid) throws Exception;

    /**
     * 查找管理员
     * @param name
     * @param password
     * @return
     */
    Manager findManager(String name , String password) throws Exception;

    /**
     * 根据id删除商品
     */
    void deleteGoodById(int id) throws Exception;

    /**
     * 更新商品信息
     */
    void updateGood(Good good) throws Exception;

    /**
     * 插入商品信息
     */
    void insertGood(Good good) throws Exception;

    /**
     * 根据分页查找用户
     * @param 
     * @return
     */
    PageBean findUsersByPage(int curPage) throws Exception;

    /**
     * 更新用户信息
     */
    void updateUser(User user) throws Exception;

    /**
     * 根据用户id删除用户
     */
    void deleteUserById(int id) throws Exception;

    /**
     * 查询用户购买记录的详细信息
     */
    List<UserShopTail> findUserShopTail(String list) throws Exception;
    
}
