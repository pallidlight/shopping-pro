package cn.gdpu.service.impl;

import cn.gdpu.dao.*;
import cn.gdpu.domain.*;
import cn.gdpu.service.ServiceDao;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import javax.swing.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ServiceDaoImpl implements ServiceDao{

    private InputStream is;
    private UserDao userDao;
    private GoodDao goodDao;
    private UserCartDao userCartDao;
    private UserShopDao userShopDao;
    private ManagerDao managerDao;
    private UserShopTailDao userShopTailDao;
    private SqlSession session;

    /**
     * 查询一个用户
     * @param name
     * @param password
     * @return
     * @throws Exception
     */
    @Override
    public User findOne(String name , String password) throws Exception{
        daoInit();
        HashMap<String,String> map = new HashMap<>();
        map.put("name" , name);
        map.put("password" , password);
        User user = new User();
        user = userDao.findOne(map);
        close(session , is);
        return user;
    }

    /**
     * 插入用户信息
     * @param user
     * @return
     * @throws Exception
     */
    @Override
    public int insert(User user) throws Exception{
        daoInit();
        userDao.insert(user);
        close(session , is);
        return 1;
    }

    /**
     * 根据uid查找用户密码
     * @param id
     */
    @Override
    public int findPasswordById(int id){
        return 0;
    }

    /**
     * 查询所有商品信息
     * @return
     */
    @Override
    public PageBean findGoodsByPage(PageBean pb) throws Exception{
        daoInit();
        HashMap<String,Integer> map = new HashMap<>();

        int start = (pb.getCurrentPage() - 1) * pb.getPageSize(); //默认每条6页
        int totalCount;
        map.put("start" , start);
        map.put("pageSize" , pb.getPageSize());
        totalCount = goodDao.findAll();
        pb.setTotalCount(totalCount);
        List<Good> list = goodDao.findGoodsByPage(map);
        pb.setList(list);
        //设置总页数 = 总记录数/每页显示条数 刚好除完就是商 不然就商+1
        int totalPage = totalCount % pb.getPageSize() == 0 ? totalCount / pb.getPageSize() : totalCount / pb.getPageSize() + 1;
        pb.setTotalPage(totalPage);
        close(session , is);
        return pb;
    }

    /**
     *查询购物表中该商品是否存在 
     * @param uc
     */
    @Override
    public int checkUserCart(UserCart uc){
        daoInit();
        int count = userCartDao.findOne(uc.getGid());
        return count;
    }

    /**
     *更新购物车的东西 
     * @param uc
     */
    @Override
    public void update(UserCart uc) throws Exception{
        daoInit();
        int count = checkUserCart(uc);
        System.out.println(count);
        if(count >= 1){
            userCartDao.update(uc);
            System.out.println("商品已存在，更新数量成功");
        }else{
            userCartDao.insert(uc);
            System.out.println("商品不存在，插入商品成功");
        }
        close(session , is);
    }

    /**
     *查询用户购物车所存在的东西
     * @param uid
     */
    @Override
    public List<UserCart> findAllOfCart(int uid) throws Exception{
        daoInit();
        List<UserCart> list = userCartDao.findAllOfCart(uid);
        close(session , is);
        return list;
    }

    /**
     * 分页查询
     * @param uid
     * @param curPage
     * @return
     */
    @Override
    public PageBean findByPage(int uid , int curPage) throws Exception{
        daoInit();
        int start = 0;
        PageBean<Object> pb = new PageBean<>();
        if(curPage > 0){
            start = (curPage - 1) * 5;  //固定一页显示5条数据
        }
        pb.setPageSize(5);
        HashMap<String,Integer> map = new HashMap<>();
        map.put("uid" , uid);
        map.put("start" , start);
        int totalCount = userCartDao.findTotalCount(uid);
        pb.setTotalCount(totalCount);
        pb.setCurrentPage(curPage);
        int totalPage = totalCount % pb.getPageSize() == 0 ? totalCount / pb.getPageSize() : totalCount / pb.getPageSize() + 1;
        pb.setTotalPage(totalPage);
        List<UserCart> list = userCartDao.findByPage(map);
        pb.setList(list);
        close(session , is);
        return pb;
    }


    /**
     * 删除购物表的东西
     * @param gid
     */
    @Override
    public void delete(int gid) throws Exception{
        daoInit();
        userCartDao.deleteOne(gid);
        close(session , is);
    }

    /**
     * 根据名字查询商品
     * @param name
     */
    @Override
    public List<Good> findGoodByName(String name) throws Exception{
        daoInit();
        List<Good> list = null;
        if("".equals(name)){
            JOptionPane.showMessageDialog(null , "请先输入查找信息!" , "提示" , JOptionPane.INFORMATION_MESSAGE);
        }else{
            list = goodDao.findOneByName(name);
            close(session , is);
        }
        return list;
    }

    /**
     * 插入购物记录
     * @param us
     */
    @Override
    public void insertUserShop(UserShop us , List<UserCart> uc) throws Exception{
        daoInit();
        userShopDao.insert(us);
        List<UserShopTail> list = new ArrayList<>();
        for(UserCart cart : uc){
            UserShopTail shopTail = new UserShopTail();
            shopTail.setUid(us.getUid());
            shopTail.setGid(cart.getGid());
            shopTail.setGname(cart.getName());
            shopTail.setGprice(cart.getPrice());
            shopTail.setGquatity(cart.getQuatity());
            shopTail.setDate(us.getDate());
            shopTail.setList(us.getList());
            list.add(shopTail);
        }
        for(UserShopTail ust : list){
            userShopTailDao.insert(ust);
        }
        userCartDao.deleteAll(us.getUid());
        for(UserCart u : uc){
            goodDao.update(u);
        }
        close(session , is);
    }

    /**
     * 查看购物记录
     * @param uid
     */
    @Override
    public List<UserShop> findAll(int uid) throws Exception{
        daoInit();
        List<UserShop> list = userShopDao.findAll(uid);
        close(session , is);
        return list;
    }

    /**
     * 查找管理员
     * @param name
     * @param password
     * @return
     */
    @Override
    public Manager findManager(String name , String password) throws Exception{
        daoInit();
        HashMap<String,String> map = new HashMap<>();
        map.put("name" , name);
        map.put("password" , password);
        Manager manager = new Manager();
        manager = managerDao.findOne(map);
        close(session , is);
        return manager;
    }

    /**
     * 根据id删除商品
     * @param id
     */
    @Override
    public void deleteGoodById(int id) throws Exception{
        daoInit();
        goodDao.deleteOne(id);
        close(session , is);
    }

    /**
     * 更新商品信息
     * @param good
     */
    @Override
    public void updateGood(Good good) throws Exception{
        daoInit();
        goodDao.updateGood(good);
        close(session , is);
    }

    /**
     * 插入商品信息
     * @param good
     */
    @Override
    public void insertGood(Good good) throws Exception{
        daoInit();
        goodDao.insertGood(good);
        close(session , is);
    }

    /**
     * 根据分页查找用户
     * @param
     * @return
     */
    @Override
    public PageBean findUsersByPage(int curPage) throws Exception{
        daoInit();
        PageBean<Object> pb = new PageBean<>();
        int start = (curPage - 1) * 5;  //固定一页显示5条数据
        pb.setPageSize(5);
        HashMap<String,Integer> map = new HashMap<>();
        map.put("start" , start);
        int totalCount = userDao.findAll();
        pb.setTotalCount(totalCount);
        pb.setCurrentPage(curPage);
        int totalPage = totalCount % pb.getPageSize() == 0 ? totalCount / pb.getPageSize() : totalCount / pb.getPageSize() + 1;
        pb.setTotalPage(totalPage);
        List<User> list = userDao.findByPage(map);
        pb.setList(list);
        close(session , is);
        return pb;
    }

    /**
     * 更新用户信息
     * @param user
     */
    @Override
    public void updateUser(User user) throws Exception{
        daoInit();
        userDao.update(user);
        close(session , is);
    }

    /**
     * 根据用户id删除用户
     * @param id
     */
    @Override
    public void deleteUserById(int id) throws Exception{
        daoInit();
        userDao.delete(id);
        close(session , is);
    }

    /**
     * 查询用户购买记录的详细信息
     * @param list
     */
    @Override
    public List<UserShopTail> findUserShopTail(String list) throws Exception{
        daoInit();
        List<UserShopTail> shopTails = userShopTailDao.findAll(list);
        close(session , is);
        return shopTails;
    }

    /**
     * 初始化相关的代理对象
     */
    public void daoInit(){
        try{
            //1.读取配置文件，生成字节输入流
            is = Resources.getResourceAsStream("SqlMapConfig.xml");
            //2.获取SqlSessionFactory
            SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
            session = factory.openSession(true);  //设置自动提交事务
            //3.使用工厂对象，创建dao对象
            userDao = session.getMapper(UserDao.class);
            goodDao = session.getMapper(GoodDao.class);
            userCartDao = session.getMapper(UserCartDao.class);
            userShopDao = session.getMapper(UserShopDao.class);
            managerDao = session.getMapper(ManagerDao.class);
            userShopTailDao = session.getMapper(UserShopTailDao.class);
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * 关闭资源
     * @param session
     * @param is
     * @throws Exception
     */
    public void close(SqlSession session , InputStream is) throws Exception{
        session.close();
        is.close();
    }
}
