package cn.gdpu.domain;

import java.io.Serializable;

public class UserCart implements Serializable{
    
    private int uid;
    private int gid;
    private String name;
    private int price;
    private int quatity;

    public int getUid(){
        return uid;
    }

    public void setUid(int uid){
        this.uid = uid;
    }

    public int getGid(){
        return gid;
    }

    public void setGid(int gid){
        this.gid = gid;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getPrice(){
        return price;
    }

    public void setPrice(int price){
        this.price = price;
    }

    public int getQuatity(){
        return quatity;
    }

    public void setQuatity(int quatity){
        this.quatity = quatity;
    }

    @Override
    public String toString(){
        return "UserCart{" +
                "uid=" + uid +
                ", gid=" + gid +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", quatity=" + quatity +
                '}';
    }
}
