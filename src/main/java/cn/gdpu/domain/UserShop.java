package cn.gdpu.domain;

import java.io.Serializable;
import java.util.Date;

public class UserShop implements Serializable{

    private int uid;
    private String list;
    private int cost;
    private Date date;

    public int getUid(){
        return uid;
    }

    public void setUid(int uid){
        this.uid = uid;
    }

    public String getList(){
        return list;
    }

    public void setList(String list){
        this.list = list;
    }

    public int getCost(){
        return cost;
    }

    public void setCost(int cost){
        this.cost = cost;
    }

    public Date getDate(){
        return date;
    }

    public void setDate(Date date){
        this.date = date;
    }

    @Override
    public String toString(){
        return "UserShop{" +
                "uid=" + uid +
                ", list='" + list + '\'' +
                ", cost=" + cost +
                ", date=" + date +
                '}';
    }
}
