package cn.gdpu.domain;

import java.io.Serializable;
import java.util.Date;

public class UserShopTail implements Serializable{
    
    private Integer uid;
    private Integer gid;
    private String gname;
    private Integer gprice;
    private Integer gquatity;
    private Date date;
    private String list;

    public Integer getUid(){
        return uid;
    }

    public void setUid(Integer uid){
        this.uid = uid;
    }

    public Integer getGid(){
        return gid;
    }

    public void setGid(Integer gid){
        this.gid = gid;
    }

    public String getGname(){
        return gname;
    }

    public void setGname(String gname){
        this.gname = gname;
    }

    public Integer getGprice(){
        return gprice;
    }

    public void setGprice(Integer gprice){
        this.gprice = gprice;
    }

    public Integer getGquatity(){
        return gquatity;
    }

    public void setGquatity(Integer gquatity){
        this.gquatity = gquatity;
    }

    public Date getDate(){
        return date;
    }

    public void setDate(Date date){
        this.date = date;
    }

    public String getList(){
        return list;
    }

    public void setList(String list){
        this.list = list;
    }

    @Override
    public String toString(){
        return "UserShopTail{" +
                "uid=" + uid +
                ", gid=" + gid +
                ", gname='" + gname + '\'' +
                ", gprice=" + gprice +
                ", gquatity=" + gquatity +
                ", date=" + date +
                ", list='" + list + '\'' +
                '}';
    }
}
