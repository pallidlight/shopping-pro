package cn.gdpu.domain;

import java.io.Serializable;
import java.util.List;

public class PageBean<T> implements Serializable{

    private int totalCount;//总记录
    private int totalPage; //总页数
    private int currentPage;//当前页数
    private int pageSize; //每页显示的条数 
    private List list; //每页展示的集合

    public int getTotalCount(){
        return totalCount;
    }

    public void setTotalCount(int totalCount){
        this.totalCount = totalCount;
    }

    public int getTotalPage(){
        return totalPage;
    }

    public void setTotalPage(int totalPage){
        this.totalPage = totalPage;
    }

    public int getCurrentPage(){
        return currentPage;
    }

    public void setCurrentPage(int currentPage){
        this.currentPage = currentPage;
    }

    public int getPageSize(){
        return pageSize;
    }

    public void setPageSize(int pageSize){
        this.pageSize = pageSize;
    }

    public List getList(){
        return list;
    }

    public void setList(List list){
        this.list = list;
    }

    @Override
    public String toString(){
        return "PageBean{" +
                "totalCount=" + totalCount +
                ", totalPage=" + totalPage +
                ", currentPage=" + currentPage +
                ", pageSize=" + pageSize +
                ", list=" + list +
                '}';
    }
}
