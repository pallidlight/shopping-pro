package cn.gdpu.domain;

import java.io.Serializable;

public class Good implements Serializable{
    private Integer id;    //商品id
    private String name;    //商品名
    private Integer price;  //单价
    private Integer stock; //库存量

    public Integer getId(){
        return id;
    }

    public void setUid(Integer id){
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public Integer getPrice(){
        return price;
    }

    public void setPrice(Integer price){
        this.price = price;
    }

    public Integer getStock(){
        return stock;
    }

    public void setStock(Integer stock){
        this.stock = stock;
    }

    @Override
    public String toString(){
        return "Good{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", stock=" + stock +
                '}';
    }
}
