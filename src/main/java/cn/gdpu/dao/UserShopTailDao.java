package cn.gdpu.dao;

import cn.gdpu.domain.UserShopTail;

import java.util.List;

public interface UserShopTailDao{
    
    void insert(UserShopTail ust);
    
    List<UserShopTail> findAll(String list);
}
