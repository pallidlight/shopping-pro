package cn.gdpu.dao;

import cn.gdpu.domain.Good;
import cn.gdpu.domain.PageBean;
import cn.gdpu.domain.User;

import javax.jws.soap.SOAPBinding;
import java.util.HashMap;
import java.util.List;

public interface UserDao {
    /**
     * 查找单个用户
     * @param map
     * @return
     */
    User findOne(HashMap<String,String > map);

    /**
     * 插入用户信息
     * @param user
     */
    void insert(User user);

   int findAll();
   
   List<User> findByPage(HashMap<String,Integer> map);
   
   void update(User user);
   
   void delete(int id);
}
