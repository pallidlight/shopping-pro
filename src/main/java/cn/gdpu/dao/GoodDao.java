package cn.gdpu.dao;

import cn.gdpu.domain.Good;
import cn.gdpu.domain.PageBean;
import cn.gdpu.domain.UserCart;

import java.util.HashMap;
import java.util.List;

public interface GoodDao{

    /**
     * 查询所有商品信息
     * @return
     */
    List<Good> findGoodsByPage(HashMap<String ,Integer> map);
    
    int findAll();
    
    List<Good> findOneByName(String name);
    
    void update(UserCart uc);
    
    void deleteOne(int id);
    
    void updateGood(Good good);
    
    void insertGood(Good good);
}
