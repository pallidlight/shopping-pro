package cn.gdpu.dao;

import cn.gdpu.domain.UserShop;

import java.util.List;

public interface UserShopDao{

    /**
     * 插入购物记录
     * @param us
     */
    void insert(UserShop us);

    /**
     * 查看购物记录
     */
    List<UserShop> findAll(int uid);
}
