package cn.gdpu.dao;

import cn.gdpu.domain.UserCart;

import java.util.HashMap;
import java.util.List;

public interface UserCartDao{
    
    List<UserCart> findAllOfCart(int uid);
    
    List<UserCart> findByPage(HashMap<String,Integer> map);
    
    int findOne(int gid);
    
    void update(UserCart uc);
    
    void insert(UserCart uc);
    
    void  deleteOne(int gid);
    
    void deleteAll(int uid);
    
    int findTotalCount(int uid);
    
}
