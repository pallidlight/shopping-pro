package cn.gdpu.dao;

import cn.gdpu.domain.Manager;
import cn.gdpu.domain.User;

import java.util.HashMap;

public interface ManagerDao{
    /**
     * 查找单个用户
     * @param map
     * @return
     */
    Manager findOne(HashMap<String,String> map);

    /**
     * 插入用户信息
     * @param manager
     */
    void insert(Manager manager);
}
