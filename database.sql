﻿-- 创建user表
create table user(
	id int PRIMARY key auto_increment,
	name varchar(20) not null,
	password varchar(14) not null
	);
	
-- 创建物品表
CREATE table goods(
	id int PRIMARY key auto_increment,
	name varchar(50) not null,
	price int not null,
	stock int not null 
	);

-- 创建购物记录表
create table user_shop(
	uid int primary key,
	list varchar(100)
	cost int,
	date datetime,
	);
	
-- 创建购物车表
create table user_cart(
	uid int primary key,
	gid int,
	name varchar(50),
	price int ,
	stock int 
	);

-- 创建购物详细记录表
CREATE TABLE `user_shop_tail`  (
  `uid` int(11) NOT NULL,
  `gid` int(11) DEFAULT NULL,
  `gname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `gprice` int(10) DEFAULT NULL,
  `gquatity` int(10) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `list` varchar(35) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) 

INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (1, '《三国演义》', 30, 45);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (2, '《红楼梦》', 40, 72);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (3, '《水浒传》', 28, 33);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (4, '《西游记》', 43, 88);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (5, '《疯狂Java讲义》', 78, 109);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (6, '256G固态硬盘', 300, 128);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (7, '《Effective Java》', 68, 99);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (8, '联想电脑(32G大容量U盘+256G固态)', 4800, 18);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (9, '《活着》', 28, 48);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (10, '雷蛇鼠标', 108, 33);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (11, '《深入解析Spring全家桶》', 198, 56);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (12, '《Java 核心技术Ⅰ》', 78, 68);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (13, '《Java 核心技术Ⅱ》', 78, 77);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (14, 'LOL全球S9总决赛门票', 10588, 3);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (15, 'DNF天空套(普雷毕业+红12)', 3000, 4);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (16, '明日方舟(满级号)', 10, 8);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (17, '《简爱》', 19, 47);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (18, '《哈利波特全集小说》(附赠精美礼包)', 288, 38);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (19, '《深入理解JVM》', 89, 56);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (20, '《编程思想》', 48, 43);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (21, '明日方舟精美艾雅法拉手办', 1088, 32);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (22, 'LOL精美阿狸手办(送游戏皮肤)', 688, 45);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (23, '《追风筝的人》', 29, 76);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (24, '《梦的解析》(弗洛伊德著)', 56, 104);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (25, '《天才在左,疯子在右》(全集)', 88, 79);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (26, '《深入MySql》', 48, 56);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (27, '《C语言 从入门到放弃》', 68, 18);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (28, '《C++高级手册》', 48, 109);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (29, '《python入门手册》', 38, 78);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (30, '《C语言之深入指针》', 58, 66);
INSERT INTO `goods`(`id`, `name`, `price`, `stock`) VALUES (31, '2020LOL全球总决赛门票', 40888, 3);

INSERT INTO `manager`(`id`, `name`, `password`) VALUES (1, 'hellott', '1234567');